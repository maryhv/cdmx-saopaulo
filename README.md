# Análisis estructural de redes de calles: comparativo entre Sao Paulo y CDMX


	novedad de hoy y ruina de pasado mañana, enterrda y resucitada cada día,
	convivida en calles, plazas, autobuses, taxis, cines, teatros, bares, hoteles, palomares, catacumbas,
	la ciudad enorme que cabe en un cuarto de tres metros cuadrados inacabable como una galaxia,
	la ciudad que nos sueña a todos y que todos hacemos y deshacemos y rehacemos mientras soñamos,
	la ciudad que todos soñamos y que cambia sin cesar mientras la soñamos
	[...]
	Octavio Paz


## Introducción

Toda estructura es una manifestación de procesos subyacentes (capra et
luisi, 2015). Una red de calles es una estructura generada por
procesos subyacentes de movilidad. Personas moviéndose y moviendo
cosas conectan el territorio.

Las ciudades son ecosistemas complejos. Ofrecen a sus habitantes
oportunidades de comercio, educación, servicios de salud, seguridad y
vida social. El soporte de estas actividades y sus beneficios
asociados requiere de enormes flujos de recursos.

Reorganizar las estructuras que vinculan aspectos biofísicos con
bienestar social y los buenos vivires es un reto prioritario de la
planificación urbana en todo el mundo.  (Ulgiati et Zucaro, 2019).

La disponibilidad de datos y bibliotecas de software hacen posible la
creación de modelos computacionales sobre los cuáles hacer búsquedas
exhaustivas, refinamientos a los hallazgos, y despliegue de datos en
visualizaciones y otros instrumentos de análisis.

Este estudio consiste de un análisis exploratorio de las redes de
calles de las dos regiones metropolitanas más grandes de América
Mestiza.


## Metodología: Red de calles como grafo

A partir de mapas digitalizados de las calles se construye una red,
creando nodos donde calles intersectan otras calles o donde dan la
vuelta, y vínculos son los tramos de calle transitable que unen esos
nodos. Los vínculos y nodos pueden clasificarse por su capacidad de
ser transitados por peatones, bicicletas o automóviles.

El orden de la red de calles se revela con medidas de entropía y otras
métricas de su topología.

foot: la teoría de grafos de donde abreva la metodología que
se usa en este estudio tuvo su origen precisamente en el análisis de
la red de calles y puentes de la ciudad de Königsberg, llevado a cabo
por Leonhard Euler. [ref]



## Obtención de Datos

Descomponemos el territorio en municipios y prefeituras. Los
analizamos por separado, junto con rasgos de sus territorios.

Hicimos este análisis a partir de datos públicos obtenidos del Open
Street Map. Las redes de calles se descargaron y pre-procesaron
automáticamente usando la biblioteca OSMNX.


Campinas, 
São José dos Campos, 
Sorocaba, 
Santos, 
Piracicaba, 
Jundiaí, 


Vila Formosa, 
Aricanduva, 
Mooca, 
Butantã, 
Parelheiros, 
Campo Limpo, 
Penha, 
Socorro, 
Anhanguera, 
Casa Verde, 
Cachoeirinha, 
Pinheiros, 
Cidade Ademar, 
Pirituba, 
Jaraguá, 
Cidade Tiradentes, 
Sé
Ermelino Matarazzo, 
Santana, 
Tucuruvi, 
Brasilândia, 
Freguesia do Ó, 
Jaçanã, 
Tremembé, 
Guaianases, 
Santo Amaro, 
Ipiranga, 
São Mateus, 
Itaim Paulista, 
São Miguel Paulista, 
Itaquera, 
Sapopemba, 
Jabaquara, 
Vila Guilherme, 
Vila Maria, 
Lapa, 
Vila Mariana, 
Jardim São Luís, 
Vila Prudente



alcaldias
{'place_name': 'Álvaro Obregón, Mexico City, Mexico'},
{'place_name': 'Azcapotzalco, Mexico City, Mexico'},
{'place_name': 'Benito Juárez, Mexico City, Mexico'},
{'place_name': 'Coyoacán, Mexico City, Mexico'},
{'place_name': 'Cuajimalpa de Morelos'},
{'place_name': 'Cuauhtémoc, Mexico City, Mexico'},
{'place_name': 'Gustavo A. Madero, Mexico City, Mexico'},
{'place_name': 'Iztacalco, Mexico City, Mexico'},
{'place_name': 'Iztapalapa, Mexico City, Mexico'},
{'place_name': 'Magdalena Contreras, Mexico City, Mexico'},
{'place_name': 'Miguel Hidalgo, Mexico City, Mexico'},
{'place_name': 'Milpa Alta, Mexico City, Mexico'},
{'place_name': 'Tláhuac, Mexico City, Mexico'},
{'place_name': 'Tlalpan, Mexico City, Mexico'},
{'place_name': 'Venustiano Carranza, Mexico City, Mexico'},
{'place_name': 'Xochimilco, Mexico City, Mexico'},


Municipios

{'place_name': 'Acolman, Mexico'},
{'place_name': 'Atenco, Mexico'},
{'place_name': 'Atizapán de Zaragoza, Mexico'},
{'place_name': 'Chalco, Mexico'},
{'place_name': 'Chiautla, Mexico'},
{'place_name': 'Chicoloapan, Mexico'},
{'place_name': 'Chiconcuac, Mexico'},
{'place_name': 'Chimalhuacán, Mexico', 'which_result': 2},
{'place_name': 'Coacalco de Berriozábal, Mexico'},
{'place_name': 'Cocotitlán, Mexico'},
{'place_name': 'Coyotepec, Mexico'},
{'place_name': 'Cuautitlán, Mexico'},
{'place_name': 'Cuautitlán Izcalli, Mexico', 'which_result': 2},
{'place_name': 'Ecatepec de Morelos, Mexico', 'which_result': 2},
{'place_name': 'Huehuetoca, Mexico'},
{'place_name': 'Huixquilucan, Mexico'},
{'place_name': 'Ixtapaluca, Mexico', 'which_result': 2},
{'place_name': 'Jaltenco, Mexico'},
{'place_name': 'La Paz, Mexico'},
{'place_name': 'Melchor Ocampo, Mexico'},
{'place_name': 'Naucalpan de Juárez, Mexico', 'which_result': 2},
{'place_name': 'Nextlalpan, Mexico'},
{'place_name': 'Nezahualcoyotl, Mexico', 'which_result': 2},
{'place_name': 'Nicolás Romero, Mexico'},
{'place_name': 'Papalotla, Mexico'},
{'place_name': 'San Martín de las Pirámides, Mexico'},
{'place_name': 'Tecámac, Mexico'},
{'place_name': 'Temamatla, Mexico'},
{'place_name': 'Teoloyucán, Mexico'},
{'place_name': 'Teotihuacán, Mexico', 'which_result': 2},
{'place_name': 'Tepetlaoxtoc, Mexico'},
{'place_name': 'Tepotzotlán, Mexico'},
{'place_name': 'Texcoco, Mexico'},
{'place_name': 'Tezoyuca, Mexico'},
{'place_name': 'Tlalmanalco, Mexico'},
{'place_name': 'Tlalnepantla de Baz, Mexico'},
{'place_name': 'Tultepec, Mexico'},
{'place_name': 'Tultitlán, Mexico'},
{'place_name': 'Valle de Chalco Solidaridad, Mexico', 'which_result': 2},
{'place_name': 'Zumpango, Mexico'},
{'place_name': 'Tizayuca, Hidalgo'}


### Análisis de datos

Para su análisis las redes se procesan por basic stats y extended
stats de osmnx. Además se construyen histogramas radiales de las
orientaciones de sus calles. Se grafican diagramas de dispersión
log-log de los grados de conectividad de los nodos de cada red. Se
crean mapas de elevación para los territorios que ocupan las
redes. Finalmente se grafican las redes distribuyendo los nodos según
sus localizaciones geográficas.


## Resultados

### Visualizaciones

Visualizar es una poderosa estrategia de análisis[9]. Se trata del
despliegue visual de mediciones a través del uso combinado de puntos,
líneas, sistemas de coordenadas, números, símbolos, palabras,
sombreado y color. Son instrumentos para el razonamiento acerca de
información cuantitativa. A veces la forma más eficaz de describir,
explorar y resumir un conjunto de datos -incluso un gran conjunto- es
mostrar imágenes de él.  Una visualización puede mostrar datos sin
distorsión, en un espacio pequeño y de manera coherente. Permite al
ojo hacer comparaciones y contrastes que resultan muy naturales, y que
son más difíciles -por ejemplo- con una tabla.

Las secciones siguientes describen resultados de estudiar las redes de
calles, apoyándose de diferentes visualizaciones.

### Análisis explorativo

Se mide la entropía de direcciones de calles, la longitud del segmento
de calle típico, la circuituidad promedio, el grado promedio de
conectividad y las proporciones de intersecciones de cuatro
direcciones y callejones sin salida.

Estos indicadores interpretados en conjunto ayudan a revelar la
extensión y complejidad de la red.

### Análisis de grados

#### Grafo completo
Como contraste considerar los grados de conectividad de los nodos en
una red en la que todos los nodos están conectados con todos los
nodos. En esta red teórica también conocida como grafo completo llegar
de cualquier punto a cualquier otro punto en la red toma sólamente un
paso. Su distribución de conectividades se ve así:

![complete graph example](images/complete_graph.png)
![k-log-log complete graph](images/k_log_log_complete_graph.png)

<https://en.wikipedia.org/wiki/Complete_graph>

Las redes de calles también tienen una distribución de
conectividad. Entre más se ajusta a la diagonal esta distribución, más
libre de escala es la red. Es decir: el mismo patrón de conectividad
se da a gran escala, a escala mediana y a pequeña escala. Una
consecuencia de este rasgo es que la red es robusta: no se disgrega
fácilmente quitando nodos. Pensando en transitar calles impedir el
paso en una red libre de escala es dificil. En un grafo completo es
imposible. En una red jerárquica es muy fácil.


#### Grafo Watts Strogatz

El caso opuesto a un grafo completo es una red vacía, o sea una red
con nodos pero sin vínculos. En una red vacía simplemente no hay
calles y es imposible ir de un lugar a otro.

Un caso casi opuesto es una red con topología de anillo. Si se está de
un lado de la red la única forma de llegar al otro lado es pasar por
todos los otros nodos y hay pocas opciones para transitar. Su gráfica
log log se ve así:

![watts strogatz ring graph](images/watts_strogatz_ring_graph.png)
![k-log-log ring graph](images/k_log_log_watts_strogatz_ring_graph.png)

ref watts strogatz?

#### Barabasi Albert

![watts strogatz ring graph](images/barabasi_albert.png)
![k-log-log ring graph](images/k_log_log_barabasi_albert.png)

ref barabasi albert



####  análisis DEM

estadística descriptiva de elevaciones, box-plot? histograms
Se trata de un sistema complejo: se despliega siempre sobre territorio moldeado por procesos biofísicos complejos.


El terreno de SP es rugoso, su red de calles es más caótica.

El terreno de CDMX era un lago, es terso, sus calles están trazadas
como una retícula regular.


## Discusion

### Ciudades y áreas conurbadas

### Tipos de redes

Para automóviles. Para peatones. Para ciclistas.


## Conclusiones

Una red de calles es en partes planificada de acuerdo a principios
claros y en partes producto de un proceso orgánico de evolución.

La medición empírica y la visualización del orden espacial ilustra
patrones y configuraciones del complejo sistema de transporte.

La medición de estos patrones de red puede ayudar a investigadores,
planificadores y a la comunidad en general a entender historias
locales de diseño urbano, planificación de transporte y morfología; a
evaluar patrones y configuraciones de transporte, y a explorar
propuestas de infraestructura y otras alternativas.

Además amplía el conocimiento acerca de ambas ciudades con un mejor
entendimiento de sus patrones urbanos y cómo se corresponden con la
evolución orgánica y el crecimiento planificado.


## Disponibilidad de datos y métodos


# Referencias

https://appliednetsci.springeropen.com/articles/10.1007/s41109-019-0189-1


[9] E. R. Tufte and P. Graves-Morris. The visual display of quantitative infor-
mation, volume 2. Graphics press Cheshire, CT, 1983.
