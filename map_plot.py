#!/usr/bin/env python

import argparse
import pickle
import osmnx as ox
from os import path
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


parser = argparse.ArgumentParser(description='Plot streets.')
parser.add_argument('infile', type=argparse.FileType('rb'))
args = parser.parse_args()

plot_name = 'map_plot_' + path.basename(args.infile.name).replace('.pickle', '.png')
dirname = 'images'

if path.isfile(path.join(dirname, plot_name)):
    print('will not overwrite found file %s' % plot_name)
    exit()

g = pickle.load(args.infile)

ox.config(log_console=True, use_cache=True)

fig, ax = ox.plot_graph(g,
                        node_size=0,
                        edge_alpha=0.77,
                        edge_color='dimgray',
                        dpi=100,
                        fig_height=15)

fig.savefig(path.join(dirname,
                      plot_name), dpi=200)
