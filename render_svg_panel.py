from os import path
from data import networks
from slugify import slugify
from jinja2 import Template


dirname = 'images'


with open('panel.svg', 'r') as f:
    template = Template(f.read())

with open('panel_h.svg', 'r') as f:
    template_h = Template(f.read())


data = dict()
for place in networks.saopaulo_macrometropolis + networks.cdmx_zona_metropolitana:
    place_name = place['place_name']
    for network_type in networks.network_types:
        slug = slugify("%s, %s" % (place_name, network_type))

        with open(path.join(dirname,
                            "panel_%s.svg" % slug), 'w') as f:
            f.write(template.render(slug=slug))

        with open(path.join(dirname,
                            "panel_h_%s.svg" % slug), 'w') as f:
            f.write(template_h.render(slug=slug))
            
