#!/usr/bin/env python

import argparse
import pickle
import osmnx as ox
from os import path
from data.areas import area

parser = argparse.ArgumentParser(description='Calculate OSMNX basic stats of pickled nw.')
parser.add_argument('infile', type=argparse.FileType('rb'))
args = parser.parse_args()

slug = path.basename(args.infile.name).replace('.pickle', '')
filename = 'basic_stats_' + path.basename(args.infile.name)
dirname = path.dirname(args.infile.name)
outpath = path.join(dirname, 'stats', filename)
if path.isfile(outpath):
    exit()

g = pickle.load(args.infile)

stats = ox.basic_stats(g, area=area[slug])

with open(outpath, 'wb') as f:
    pickle.dump(stats, f)
