#!/usr/bin/env python

import argparse
import pickle
import osmnx as ox
from os import path

parser = argparse.ArgumentParser(description='Calculate OSMNX extended stats of pickled nw.')
parser.add_argument('infile', type=argparse.FileType('rb'))
args = parser.parse_args()

filename = 'extended_stats_' + path.basename(args.infile.name)
dirname = path.dirname(args.infile.name)
outpath = path.join(dirname, 'stats', filename)
if path.isfile(outpath):
    exit()


g = pickle.load(args.infile)

stats = ox.extended_stats(g, ecc=True, bc=True)

with open(outpath, 'wb') as f:
    pickle.dump(stats, f)
