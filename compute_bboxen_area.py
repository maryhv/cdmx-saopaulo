from LatLon23 import LatLon, Latitude, Longitude


print('greater_saopaulo')
north=-23.1
south=-24.0
west=-46.9
east=-46.3

nw = LatLon(Latitude(north),
            Longitude(west))

ne = LatLon(Latitude(north),
            Longitude(east))

sw = LatLon(Latitude(south),
            Longitude(west))

se = LatLon(Latitude(south),
            Longitude(east))


# height
h = ne.distance(se) * 1000
# width
w = sw.distance(se) * 1000
print("h=%s w=%s area=%s" % (h, w, h*w))



print('cdmx_zona_metropolitana')
north=20.0
south=19.1
west=-99.4
east=-98.8

nw = LatLon(Latitude(north),
            Longitude(west))

ne = LatLon(Latitude(north),
            Longitude(east))

sw = LatLon(Latitude(south),
            Longitude(west))

se = LatLon(Latitude(south),
            Longitude(east))


# height
h = nw.distance(sw) * 1000
# width
w = nw.distance(ne) * 1000
print("h=%s w=%s area=%s" % (h, w, h*w))
