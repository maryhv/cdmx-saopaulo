#!/usr/bin/env python

import argparse
import pickle
import osmnx as ox
from os import path
import matplotlib.pyplot as plt



parser = argparse.ArgumentParser(description='Radial histogram and plot streets.')
parser.add_argument('infile', type=argparse.FileType('rb'))
args = parser.parse_args()

dirname = 'images'
log_log_name = 'k_log_log_' + path.basename(args.infile.name).replace('.pickle', '.png')
outpath = path.join(dirname,
                    log_log_name)

g = pickle.load(args.infile)


fig = plt.figure(figsize=(5, 5), dpi=100)

degree_sequence=sorted(dict(g.degree()).values(), reverse=True)
f = plt.loglog(degree_sequence,
               marker='.',
               linewidth=0.3,
               color='dimgrey',
               alpha=0.3)

ax = fig.get_axes()[0]
ax.set_ylabel("nodos")
ax.set_xlabel("grado")
fig.tight_layout()
fig.savefig(outpath)
