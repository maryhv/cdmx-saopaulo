import osmnx as ox
from os import path
import pickle
from slugify import slugify

data_dir = path.dirname(__file__)

network_types = ('drive', 'walk', 'bike')

# bboxen
bboxen = [
    {'place_name': 'Greater Mexico City'},
    {'place_name': 'Greater São Paulo'},
]

# São Paulo Macrometropolis
saopaulo_prefeituras = [
    {'place_name': 'Campinas, São Paulo, Brazil'},
    {'place_name': 'São José dos Campos, São Paulo, Brazil', 'which_result': 2},
    {'place_name': 'Sorocaba, São Paulo, Brazil'},
    {'place_name': 'Santos, São Paulo, Brazil'},
    {'place_name': 'Piracicaba, São Paulo, Brazil'},
    {'place_name': 'Jundiaí, São Paulo, Brazil'},
    {'place_name': 'Vila Formosa, São Paulo'},
    {'place_name': 'Aricanduva, São Paulo'},
    {'place_name': 'Mooca, São Paulo'},
    {'place_name': 'Butantã, São Paulo'},
    {'place_name': 'Parelheiros, São Paulo'},
    {'place_name': 'Campo Limpo, São Paulo'},
    {'place_name': 'Penha, São Paulo'},
    {'place_name': 'Socorro, São Paulo', 'which_result': 2},
    {'place_name': 'Anhanguera, São Paulo'},
    {'place_name': 'Casa Verde, São Paulo'},
    {'place_name': 'Cachoeirinha, São Paulo'},
    {'place_name': 'Pinheiros, São Paulo', 'which_result': 4},
    {'place_name': 'Cidade Ademar, São Paulo'},
    {'place_name': 'Pirituba, São Paulo'},
    {'place_name': 'Jaraguá, São Paulo'},
    {'place_name': 'Cidade Tiradentes, São Paulo'},
    {'place_name': 'se centro, sao paulo, brazil'},
    {'place_name': 'Ermelino Matarazzo, São Paulo'},
    {'place_name': 'Santana, São Paulo'},
    {'place_name': 'Tucuruvi, São Paulo', 'which_result': 3},
    {'place_name': 'Brasilândia, São Paulo'},
    {'place_name': 'Freguesia do Ó, São Paulo'},
    {'place_name': 'Jaçanã, São Paulo'},
    {'place_name': 'Tremembé, São Paulo'},
    {'place_name': 'Guaianases, São Paulo'},
    {'place_name': 'Santo Amaro, São Paulo', 'which_result': 3},
    {'place_name': 'Ipiranga, São Paulo'},
    {'place_name': 'São Mateus, São Paulo'},
    {'place_name': 'Itaim Paulista, São Paulo'},
    {'place_name': 'São Miguel Paulista, São Paulo'},
    {'place_name': 'Itaquera, São Paulo'},
    {'place_name': 'Sapopemba, São Paulo'},
    {'place_name': 'Jabaquara, São Paulo'},
    {'place_name': 'Vila Guilherme, São Paulo'},
    {'place_name': 'Vila Maria, São Paulo'},
    {'place_name': 'Lapa, São Paulo'},
    {'place_name': 'Vila Mariana, São Paulo'},
    {'place_name': "Jardim São Luís, São Paulo"},
    {'place_name': 'Vila Prudente'},
]


alcaldias_municipios = [
    {'place_name': 'Álvaro Obregón, Mexico City, Mexico'},
    {'place_name': 'Azcapotzalco, Mexico City, Mexico'},
    {'place_name': 'Benito Juárez, Mexico City, Mexico'},
    {'place_name': 'Coyoacán, Mexico City, Mexico'},
    {'place_name': 'Cuajimalpa de Morelos'},
    {'place_name': 'Cuauhtémoc, Mexico City, Mexico'},
    {'place_name': 'Gustavo A. Madero, Mexico City, Mexico'},
    {'place_name': 'Iztacalco, Mexico City, Mexico'},
    {'place_name': 'Iztapalapa, Mexico City, Mexico'},
    {'place_name': 'Magdalena Contreras, Mexico City, Mexico'},
    {'place_name': 'Miguel Hidalgo, Mexico City, Mexico'},
    {'place_name': 'Milpa Alta, Mexico City, Mexico'},
    {'place_name': 'Tláhuac, Mexico City, Mexico'},
    {'place_name': 'Tlalpan, Mexico City, Mexico'},
    {'place_name': 'Venustiano Carranza, Mexico City, Mexico'},
    {'place_name': 'Xochimilco, Mexico City, Mexico'},
    {'place_name': 'Acolman, Mexico'},
    {'place_name': 'Atenco, Mexico'},
    {'place_name': 'Atizapán de Zaragoza, Mexico'},
    {'place_name': 'Chalco, Mexico'},
    {'place_name': 'Chiautla, Mexico'},
    {'place_name': 'Chicoloapan, Mexico'},
    {'place_name': 'Chiconcuac, Mexico'},
    {'place_name': 'Chimalhuacán, Mexico', 'which_result': 2},
    {'place_name': 'Coacalco de Berriozábal, Mexico'},
    {'place_name': 'Cocotitlán, Mexico'},
    {'place_name': 'Coyotepec, Mexico'},
    {'place_name': 'Cuautitlán, Mexico'},
    {'place_name': 'Cuautitlán Izcalli, Mexico', 'which_result': 2},
    {'place_name': 'Ecatepec de Morelos, Mexico', 'which_result': 2},
    {'place_name': 'Huehuetoca, Mexico'},
    {'place_name': 'Huixquilucan, Mexico'},
    {'place_name': 'Ixtapaluca, Mexico', 'which_result': 2},
    {'place_name': 'Jaltenco, Mexico'},
    {'place_name': 'La Paz, Mexico'},
    {'place_name': 'Melchor Ocampo, Mexico'},
    {'place_name': 'Naucalpan de Juárez, Mexico', 'which_result': 2},
    {'place_name': 'Nextlalpan, Mexico'},
    {'place_name': 'Nezahualcoyotl, Mexico', 'which_result': 2},
    {'place_name': 'Nicolás Romero, Mexico'},
    {'place_name': 'Papalotla, Mexico'},
    {'place_name': 'San Martín de las Pirámides, Mexico'},
    {'place_name': 'Tecámac, Mexico'},
    {'place_name': 'Temamatla, Mexico'},
    {'place_name': 'Teoloyucán, Mexico'},
    {'place_name': 'Teotihuacán, Mexico', 'which_result': 2},
    {'place_name': 'Tepetlaoxtoc, Mexico'},
    {'place_name': 'Tepotzotlán, Mexico'},
    {'place_name': 'Texcoco, Mexico'},
    {'place_name': 'Tezoyuca, Mexico'},
    {'place_name': 'Tlalmanalco, Mexico'},
    {'place_name': 'Tlalnepantla de Baz, Mexico'},
    {'place_name': 'Tultepec, Mexico'},
    {'place_name': 'Tultitlán, Mexico'},
    {'place_name': 'Valle de Chalco Solidaridad, Mexico', 'which_result': 2},
    {'place_name': 'Zumpango, Mexico'},
    {'place_name': 'Tizayuca, Hidalgo'}
]


todas_as_prefeituras = saopaulo_prefeituras + alcaldias_municipios


def load(slug):
    input_path = path.join(data_dir,
                           "%s.pickle" % slug)

    with open(input_path, 'rb') as f:
        g = pickle.load(f)
        return g



def pickle_from_place(place_name,
                      network_type='drive',
                      which_result=1):

    file_name = path.join(data_dir,
                          slugify("%s, %s" % (place_name, network_type)) + '.pickle')

    if not path.isfile(file_name):
        print("downloading %s" % file_name)
        G = ox.graph_from_place(place_name,
                                network_type=network_type,
                                which_result=which_result)
        with open(file_name, 'wb') as f:
            pickle.dump(G, f)



def get_or_create(place_name,
                  network_type='drive',
                  which_result=1):
    """
    If a local pickle file is found, load and return it.
    Else download street network by place name and network type,
    save it to a local pickle file and return it.

    'which_result': see osmnx docs
    """
    file_name = path.join(data_dir,
                          slugify("%s, %s" % (place_name, network_type)) + '.pickle')
    if path.isfile(file_name):
        G = True
        with open(file_name, 'rb') as f:
            G =pickle.load(f)
    else:
        G = ox.graph_from_place(place_name, network_type=network_type, which_result=which_result)
        with open(file_name, 'wb') as f:
            pickle.dump(G, f)

    return G




def pickle_from_bbox(bbox, slug, network_type='all'):

    file_name = path.join(data_dir,
                          "%s.pickle" % slug)
    if path.isfile(file_name):
        exit()

    north, south, east, west = bbox
    print(north, west)
    G = ox.graph_from_bbox(north, south, east, west,
                           clean_periphery=True,
                           simplify=True,
                           network_type=network_type)
    G = ox.project_graph(G)

    with open(file_name, 'wb') as f:
        pickle.dump(G, f)
