from data import networks

for place in networks.saopaulo_macrometropolis + networks.cdmx_zona_metropolitana:
    for nw_type in networks.network_types:
        networks.pickle_from_place(place_name=place['place_name'],
                                   network_type=nw_type,
                                   which_result=place.get('which_result', 1))
